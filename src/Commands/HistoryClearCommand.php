<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Log\Command\FileLog;
use Jakmall\Recruitment\Calculator\History\Log\Command\DatabaseLog;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            'history:clear',
            $commandVerb
        );
        $this->description = sprintf('Clear %s', $commandVerb);

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history';
    }

    public function handle(): void
    {
        $database = new DatabaseLog;
        $clearDatabase = $database->clearAll();

        $file = new FileLog;
        $clearFile = $file->clearAll();

        if($clearDatabase && $clearFile)
        {
            $this->info("History cleared!");
        }
        else
        {
            $this->info("Failed clear history!");
        }
    }
}
