<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class StarCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $this->signature = 'star {number}';
        $this->description = 'Star';

        parent::__construct();
    }

    public function handle(): void
    {
      $number = $this->argument('number');

      $star = '*';
      for($i=0; $i<$number; $i++)
      {
        $this->comment($star);
        $star .= '*';
      }
    }
}
