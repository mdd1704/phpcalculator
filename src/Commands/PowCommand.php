<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Log\Command\FileLog;
use Jakmall\Recruitment\Calculator\History\Log\Command\DatabaseLog;

class PowCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {base : The base number} {exp : The exp number}',
            $commandVerb
        );
        $this->description = sprintf('Exponent the given number');

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'pow';
    }

    public function handle(): void
    {
        $base = $this->getBase();
        $exp = $this->getExp();
        $description = $this->generateCalculationDescription($base, $exp);
        $result = $this->calculate($base, $exp);

        $this->comment(sprintf('%s = %s', $description, $result));

        $command = array(
            'command' => ucfirst($this->getCommandVerb()),
            'description' => $description,
            'result' => $result,
            'output' => sprintf('%s = %s', $description, $result)
        );

        $file = new FileLog;
        $file->log($command);

        $db = new DatabaseLog;
        $db->log($command);
    }

    protected function getBase(): int
    {
        return $this->argument('base');
    }

    protected function getExp(): int
    {
        return $this->argument('exp');
    }

    protected function generateCalculationDescription(int $base, int $exp): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return $base . $glue . $exp;
    }

    protected function getOperator(): string
    {
        return '^';
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($base, $exp)
    {
        return pow($base, $exp);
    }
}
