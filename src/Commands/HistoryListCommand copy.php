<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Log\Command\FileLog;
use Jakmall\Recruitment\Calculator\History\Log\Command\DatabaseLog;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            'history:list {commands?* : Filter the %s by commands} {--D|driver=file : Driver for storage connection}',
            $commandVerb
        );
        $this->description = sprintf('Show calculator %s', $commandVerb);

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history';
    }

    public function handle(): void
    {
        $commands = $this->getCommands();
        $driver = $this->getDriver();

        if($driver == 'database')
        {
            $database = new DatabaseLog;
            $logs = $database->findAll();
        }
        else
        {
            $file = new FileLog;
            $logs = $file->findAll();
        }

        if(count($logs) > 0)
        {
            $headers = ['No', 'Command', 'Description', 'Result', 'Output', 'Time'];
            $data = [];
            $i = 0;
    
            foreach($logs as $log)
            {
                $check = strtolower($log->command);
    
                if(count($commands) == 0 || in_array($check, $commands))
                {
                    $data[$i] = [
                        $i+1, $log->command, $log->description, $log->result, $log->output, $log->time
                    ];
                    $i++;
                }
            }
    
            $this->table($headers, $data);
        }
        else
        {
            $this->info('History is empty.');
        }
    }

    protected function getCommands(): array
    {
        return $this->argument('commands');
    }

    protected function getDriver(): string
    {
        return $this->option('driver');
    }
}
