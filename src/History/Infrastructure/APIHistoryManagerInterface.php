<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

//TODO: create implementation.
interface APIHistoryManagerInterface
{
    /**
     * Returns array of API history.
     *
     * @return array
     */
    public function findAll(): array;

    /**
     * Returns object of API history.
     *
     * @return array
     */
    public function find($id): object;

    /**
     * Log API data to storage.
     *
     * @param mixed $command The command to log.
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($param): bool;

    /**
     * Clear all data from storage.
     *
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clear($id): bool;
}
