<?php

namespace Jakmall\Recruitment\Calculator\History\Log\Command;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class DatabaseLog implements CommandHistoryManagerInterface
{
  public function log($command): bool
  {
    return true;
  }

  public function findAll(): array
  {
    return [];
  }

  public function clearAll():bool
  {
    return true;
  }
}