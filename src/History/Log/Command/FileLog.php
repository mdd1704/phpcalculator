<?php

namespace Jakmall\Recruitment\Calculator\History\Log\Command;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class FileLog implements CommandHistoryManagerInterface
{
  protected $filename = "log-command.json";

  public function log($command): bool
  {
    $time = date('Y-m-d H:i:s', time());
    $command['time'] = $time;

    try {
      $filename = $this->filename;
      $result = '';

      if(!is_file($filename)) file_put_contents($filename, $result);

      if(filesize($filename) > 0)
      {
        $log = fopen($filename, "r") or die("Unable to open file!");
        $result = fread($log, filesize($filename));
        $result = substr($result, 1, -1) . ',';
      }

      $log = fopen($filename, "w") or die("Unable to open file!");
      $text = $result . json_encode($command);
      $text = sprintf('[%s]', $text);

      fwrite($log, $text);
      fclose($log);
    } catch (\Exception $e) {
      return false;
    }

    return true;
  }

  public function findAll(): array
  {
    $filename = $this->filename;
    $result = '';

    if(!is_file($filename)) file_put_contents($filename, $result);

    if(filesize($filename) > 0)
    {
      try {
        $myfile = fopen($filename, "r") or die("Unable to open file!");
        $result = fread($myfile, filesize($filename));
        $object = json_decode($result);
      } catch (\Exception $e) {
        return [];
      }
    }
    else 
    {
      return [];
    }

    return $object;
  }

  public function clearAll():bool
  {
    $filename = $this->filename;

    if(is_file($filename))
    {
      try {
        $log = fopen($filename, "w") or die("Unable to open file!");\
        fwrite($log, '');
        fclose($log);
      } catch (\Exception $e) {
        return false;
      }
    }

    return true;
  }
}