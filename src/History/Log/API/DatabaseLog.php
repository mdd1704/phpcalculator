<?php

namespace Jakmall\Recruitment\Calculator\History\Log\API;

use Jakmall\Recruitment\Calculator\History\Infrastructure\APIHistoryManagerInterface;

class DatabaseLog implements APIHistoryManagerInterface
{
  public function log($param): bool
  {
    return true;
  }

  public function findAll(): array
  {
    return [];
  }

  public function find($id): object
  {
    $object = (object) array();

    return $object;
  }

  public function clear($id):bool
  {
    return true;
  }
}