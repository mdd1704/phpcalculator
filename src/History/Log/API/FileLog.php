<?php

namespace Jakmall\Recruitment\Calculator\History\Log\API;

use Jakmall\Recruitment\Calculator\History\Infrastructure\APIHistoryManagerInterface;

class FileLog implements APIHistoryManagerInterface
{
  protected $filename = "log-api.json";

  public function log($param): bool
  {
    $time = date('Y-m-d H:i:s', time());
    $param['time'] = $time;

    try {
      $filename = $this->filename;
      $result = '';
      $id = 1;

      if(!is_file($filename)) file_put_contents($filename, $result);

      if(filesize($filename) > 0)
      {
        $log = fopen($filename, "r") or die("Unable to open file!");
        $result = fread($log, filesize($filename));

        $items = json_decode($result);
        foreach($items as $key=>$item)
        {
          if($key+1 == count($items))
          {
            $id += $item->id;
          }
        }

        $result = substr($result, 1, -1) . ',';
      }

      $reverse = array_reverse($param);
      $reverse['id'] = $id;
      $data = array_reverse($reverse);

      $log = fopen($filename, "w") or die("Unable to open file!");
      $text = $result . json_encode($data);
      $text = sprintf('[%s]', $text);

      fwrite($log, $text);
      fclose($log);
    } catch (\Exception $e) {
      return false;
    }

    return true;
  }

  public function findAll(): array
  {
    $filename = $this->filename;
    $result = '';

    if(!is_file($filename)) file_put_contents($filename, $result);

    if(filesize($filename) > 0)
    {
      try {
        $myfile = fopen($filename, "r") or die("Unable to open file!");
        $result = fread($myfile, filesize($filename));
        $object = json_decode($result);
      } catch (\Exception $e) {
        return [];
      }
    }
    else 
    {
      return [];
    }

    return $object;
  }

  public function find($id): object
  {
    $object = (object) array();

    $filename = $this->filename;
    $result = '';

    if(!is_file($filename)) file_put_contents($filename, $result);

    if(filesize($filename) > 0)
    {
      try {
        $myfile = fopen($filename, "r") or die("Unable to open file!");
        $result = fread($myfile, filesize($filename));
        $items = json_decode($result);

        foreach($items as $item)
        {
          if($item->id == $id)
          {
            $object = $item;
          }
        }
      } catch (\Exception $e) {
        return [];
      }
    }
    else 
    {
      return [];
    }

    return $object;
  }

  public function clear($id):bool
  {
    try {
      $filename = $this->filename;

      if(filesize($filename) > 0)
      {
        $log = fopen($filename, "r") or die("Unable to open file!");
        $result = fread($log, filesize($filename));

        $items = json_decode($result);
        $new = array();
        foreach($items as $item)
        {
          if($item->id != $id)
          {
            array_push($new,$item);
          }
        }

        if(count($new) > 0)
        {
          $text = json_encode($new);
        }
        else
        {
          $text = '';
        }

        $log = fopen($filename, "w") or die("Unable to open file!");
        fwrite($log, $text);
        fclose($log);
      }
    } catch (\Exception $e) {
      return false;
    }

    return true;
  }
}