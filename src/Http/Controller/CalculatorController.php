<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\Log\API\FileLog;
use Jakmall\Recruitment\Calculator\History\Log\API\DatabaseLog;

class CalculatorController
{
    protected $command = array('add', 'subtract', 'multiply', 'divide', 'pow');

    public function calculate($action, Request $request)
    {
        header('Content-Type: application/json');

        if(!in_array($action, $this->command))
        {
            http_response_code(500);
            echo json_encode([
                'message' => 'The selected action is invalid.'
            ]);
        }
        else
        {
            if(!$request->input || !is_array($request->input) || count($request->input) == 0)
            {
                http_response_code(500);
                echo json_encode([
                    'message' => 'The input field is required.'
                ]);
            }
            else
            {
                $check = true;
                foreach($request->input as $item)
                {
                    if(!is_numeric($item))
                    {
                        $check = false;
                        break;
                    }
                }

                if(!$check)
                {
                    http_response_code(500);
                    echo json_encode([
                        'message' => 'The input must be an integer.'
                    ]);
                }
                else
                {
                    switch ($action) {
                        case "add":
                            self::add($request->input);
                            break;
                        case "subtract":
                            self::subtract($request->input);
                            break;
                        case "multiply":
                            self::multiply($request->input);
                            break;
                        case "divide":
                            self::divide($request->input);
                            break;
                        case "pow":
                            if($request->input[0] && $request->input[1])
                            {
                                self::pow($request->input);
                            }
                            else
                            {
                                http_response_code(500);
                                echo json_encode([
                                    'message' => 'The input[0] & input[1] field is required.'
                                ]);
                            }
                            break;
                        default:
                            self::add($request->input);
                    }
                }
            }
        }
    }

    protected function store($param, $input, $result)
    {
        unset($param['result']);
        $param['input'] = $input;
        $param['result'] = $result;

        $file = new FileLog;
        $file->log($param);

        $database = new DatabaseLog;
        $database->log($param);
    }

    protected function addCalculate($number1, $number2)
    {
        return $number1 + $number2;
    }

    protected function addCalculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->addCalculate($this->addCalculateAll($numbers), $number);
    }

    public function add($input)
    {
        $glue = sprintf(' + ');
        $operation = implode($glue, $input);
        $result = $this->addCalculateAll($input);

        $param = array(
            'command'   => 'add',
            'operation' => $operation,
            'result'    => $result
        );

        self::store($param, $input, $result);

        echo json_encode($param);
    }

    protected function subtractCalculate($number1, $number2)
    {
        return $number1 - $number2;
    }

    protected function subtractCalculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->subtractCalculate($this->subtractCalculateAll($numbers), $number);
    }

    public function subtract($input)
    {
        $glue = sprintf(' - ');
        $operation = implode($glue, $input);
        $result = $this->subtractCalculateAll($input);

        $param = array(
            'command'   => 'subtract',
            'operation' => $operation,
            'result'    => $result
        );

        self::store($param, $input, $result);

        echo json_encode($param);
    }

    protected function multiplyCalculate($number1, $number2)
    {
        return $number1 * $number2;
    }

    protected function multiplyCalculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->multiplyCalculate($this->multiplyCalculateAll($numbers), $number);
    }

    public function multiply($input)
    {
        $glue = sprintf(' * ');
        $operation = implode($glue, $input);
        $result = $this->multiplyCalculateAll($input);

        $param = array(
            'command'   => 'multiply',
            'operation' => $operation,
            'result'    => $result
        );

        self::store($param, $input, $result);

        echo json_encode($param);
    }

    protected function divideCalculate($number1, $number2)
    {
        return $number1 / $number2;
    }

    protected function divideCalculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->divideCalculate($this->divideCalculateAll($numbers), $number);
    }

    public function divide($input)
    {
        $glue = sprintf(' / ');
        $operation = implode($glue, $input);
        $result = $this->divideCalculateAll($input);

        $param = array(
            'command'   => 'divide',
            'operation' => $operation,
            'result'    => $result
        );

        self::store($param, $input, $result);

        echo json_encode($param);
    }

    public function pow($input)
    {
        $operation = $input[0] . ' ^ ' . $input[1];
        $result = pow($input[0], $input[1]);

        $param = array(
            'command'   => 'pow',
            'operation' => $operation,
            'result'    => $result
        );

        $input_pow[0] = $input[0];
        $input_pow[1] = $input[1];

        self::store($param, $input_pow, $result);

        echo json_encode($param);
    }
}
