<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;

class ExponentController
{
    public function index(Request $request)
    {
      $number = $request->number;
      $exp = $request->exp;

      $result = $this->exponent($number, $number, $exp);
      echo $result;
    }

    protected function exponent($number, $base, $exp)
    {
      if($exp == 1)
      {
        return $number;
      }
      else 
      {
        $number *= $base;

        return $this->exponent($number, $base, $exp-1);
      }
    }
}