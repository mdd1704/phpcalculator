<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;

class StarController
{
    public function index(Request $request)
    {
      $number = $request->number;

      for($i=0; $i<$number; $i++)
      {
        for($j=$number; $j>$i; $j--)
        {
          echo '_';
        }

        for($k=0; $k<=$i; $k++)
        {
          echo '*_';
        }

        echo "<br/>";
      }
    }
}