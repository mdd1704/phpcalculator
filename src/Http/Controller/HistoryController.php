<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\Log\API\FileLog;
use Jakmall\Recruitment\Calculator\History\Log\API\DatabaseLog;

class HistoryController
{
    public function index(Request $request)
    {
        header('Content-Type: application/json');

        switch($request->driver) {
            case "database":
                $driver = "database";
                break;
            case "file":
                $driver = "file";
                break;
            default:
                $driver = "file";
        }

        if($driver == "database")
        {
            $database = new DatabaseLog;
            $logs = $database->findAll();
        }
        else
        {
            $file = new FileLog;
            $logs = $file->findAll();
        }

        echo json_encode($logs);
    }

    public function show($id, Request $request)
    {
        header('Content-Type: application/json');

        switch($request->driver) {
            case "database":
                $driver = "database";
                break;
            case "file":
                $driver = "file";
                break;
            default:
                $driver = "file";
        }

        if($driver == "database")
        {
            $database = new DatabaseLog;
            $logs = $database->find($id);
        }
        else
        {
            $file = new FileLog;
            $logs = $file->find($id);
        }

        echo json_encode($logs);
    }

    public function remove($id)
    {
        $database = new DatabaseLog;
        $clearDatabase = $database->clear($id);

        $file = new FileLog;
        $clearFile = $file->clear($id);
    }
}
